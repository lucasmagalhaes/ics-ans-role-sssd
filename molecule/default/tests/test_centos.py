import os
import testinfra.utils.ansible_runner
import re

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('ics-ans-role-sssd-default-*')


def test_sssd_running_and_enabled(host):
    sssd = host.service("sssd")
    assert sssd.is_running
    assert sssd.is_enabled


def test_oddjobd_running_and_enabled(host):
    oddjobd = host.service("oddjobd")
    assert oddjobd.is_running
    assert oddjobd.is_enabled


def test_ldap_conf(host):
    ldap_conf = host.file('/etc/openldap/ldap.conf').content_string
    assert re.search(r'\nTLS_CACERTDIR\s+/etc/openldap/certs', ldap_conf) is not None


def test_pam_conf(host):
    pam_conf = host.file('/etc/pam.d/system-auth').content_string
    assert re.search(r'\nauth\s+sufficient\s+pam_sss.so', pam_conf) is not None
